#!/bin/bash
sudo apt-get update
sudo apt-get -y dist-upgrade 
sudo apt-get autoremove -y
sudo apt-get install -y --no-install-recommends \
    software-properties-common \
    wget \
    pwgen \
    net-tools \
    build-essential \
    git \
    bzip2 \
    vim \
    unzip 

# install docker	
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# tell docker to build the image
sudo docker build --tag cs510 .



