# Docker image for building/running xv6-riscv
 
FROM ubuntu as opfsbuilder
WORKDIR /root/
RUN apt-get update \
&& apt-get install -y git build-essential \
&& git clone https://github.com/titech-os/opfs.git \
&& (cd opfs; make PREFIX=/root/local install)
 
FROM ubuntu
 
LABEL maintainer="sjqi@cs.duke.edu"
 
ARG TZ=EDT
ARG USER=dukecs
ARG GROUP=dukecs
ARG PASS=dukecs
ENV HOME=/home/${USER}
ENV XV6=${HOME}/lab
 
COPY --from=opfsbuilder /root/local/bin/* /usr/local/bin/
 
RUN apt-get update \
&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata \
&& ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime \
&& dpkg-reconfigure --frontend noninteractive tzdata \
&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
sudo \
git \
build-essential \
gdb-multiarch \
qemu-system-misc \
gcc-riscv64-unknown-elf \
binutils-riscv64-unknown-elf \
python3 \
&& rm -rf /var/lib/apt/lists/* \
&& groupadd ${GROUP} \
&& useradd -g ${GROUP} -m ${USER} \
&& (echo "${USER}:${PASS}" | chpasswd) \
&& gpasswd -a ${USER} sudo \
&& mkdir -p ${XV6} \
&& chown -R ${USER}:${GROUP} ${XV6} \
&& (echo "add-auto-load-safe-path ${XV6}/.gdbinit" > ${HOME}/.gdbinit) \
&& chown ${USER}:${GROUP} ${HOME}/.gdbinit
 
USER ${USER}
WORKDIR ${XV6}
 
ENTRYPOINT ["/bin/bash"]
